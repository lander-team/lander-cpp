# LanderCpp

## System Requirements

_Note: these are only necessary if you plan on messing around with the source code. If you just want to see the end result, you should be able to go into the build/release directory and run the executable with no problems_

- Visual Studio Code / C++ Extension
- mingw64 Compiler
- gdb debugger

The best way to install is by following this guide for [Windows](https://code.visualstudio.com/docs/cpp/config-mingw) or for [Linux](https://code.visualstudio.com/docs/cpp/config-linux)

## Overview

- main.cpp contains all of user defined initial conditions
- sim.h contains all of the simulation calculations
- As of right now, the executable just spits out a csv file...
- Currently using simPlot.m to plot all of the data in the csv file. So I suggest having Matlab open while you're messing aound with this.

## Known Issues

- See issue tracker

## Results

These results are from the latest simulation automatically built with the code from the Master branch. To get code from the most recent commits go to [Pipelines](https://gitlab.com/lander-team/lander-cpp/-/pipelines) where you can download the artifacts from each commit.

Download the raw CSV: https://lander-team.gitlab.io/lander-cpp/simOut.csv

Download Linux Binaries: https://lander-team.gitlab.io/lander-cpp/sim.out

### Plots

Click on the images for interactive versions. 

<a href="https://lander-team.gitlab.io/lander-cpp/plot1.html" rel="Accel-Vel-ALt">![Accel-Vel-ALt](https://lander-team.gitlab.io/lander-cpp/plot1.svg)</a>

<a href="https://lander-team.gitlab.io/lander-cpp/plot2.html" rel="Euler Angles vs Time">![Euler Angles vs Time](https://lander-team.gitlab.io/lander-cpp/plot2.svg)</a>

<a href="https://lander-team.gitlab.io/lander-cpp/plot3.html" rel="Servo Position vs Time">![Servo Position vs Time](https://lander-team.gitlab.io/lander-cpp/plot3.svg)</a>