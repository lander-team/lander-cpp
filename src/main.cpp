#if defined(_WIN32) || defined(NATIVE)
#include <cmath>
#include <fstream>
#include <iostream>
#include <stdexcept> // std::runtime_error
#include <stdio.h>
#include <string>
#include <vector>
#elif defined(TEENSY)
#include <Arduino.h>
#endif

#define M_PI 3.14159265359
#include "Vehicle.h"
#include "sim.h"

Vehicle State;
Vehicle PrevState;

#if defined(TEENSY)

int BUILTIN_LED = 13;
unsigned long last, initTime;

#include "teensy.h"
#include <HX711.h>
#include <PWMServo.h>

PWMServo servo1;
PWMServo servo2;

const int pin_servo1 = 33;
const int pin_servo2 = 29;
const int pin_igniter = 7;

int servoPos = 90; // variable to store the servo position;

void setup() {
  Serial.begin(9600);

  delay(5000);
  Serial.println("Simulation Countdown:");
  for (int i = 0; i < 10; i++) {
    Serial.println(10 - i);
    delay(1000);
  }

  initLoadCells(State);
  init_Vehicle(State);

  servo1.attach(pin_servo1);
  servo2.attach(pin_servo2);
  testGimbal(servo1, servo2);
  Serial.println("Servos Tested");

  delay(3000);
  Serial.println("Simulated Vehicle Initalized");

  // Determine when to burn
  analogWrite(pin_igniter, 0);
  burnStartTimeCalc(State);
  Serial.println("Starting Height Calculated");

  initFile();

  // PLACE BUTTON HERE---------------------------------------------

  initTime = micros();
}

void loop() {
  last = micros();

  vehicleDynamics(State, PrevState);
  thrustInfo(State);
  pidController(State, PrevState);
  TVC(State, PrevState);
  processTVC(State, servo1, servo2);

  write2CSV(State);

  // Set previous values for next timestep
  PrevState = State;
  State.time += State.stepSize;

  if ((State.z < 0.0) && (State.thrustFiring == 2)) {
    analogWrite(pin_igniter, 0);
    Serial.println("Run duration:" + String((micros() - initTime) / 1000000.0) +
                   " seconds");
    printSimResults(State);

    closeFile();
    delay(3000);

    Serial.println("SUCCESS");
    Serial.println("Exiting Sim");
    teensyAbort();
  }

  State.stepDuration = micros() - last;
  delayMicroseconds((1000.0 * State.stepSize) - State.stepDuration);
}

#endif

#if defined(_WIN32) || defined(NATIVE)

#include "native.h"
outVector stateVector;

int main() {

  init_Vehicle(State);

  // Determine when to burn
  burnStartTimeCalc(State);

  do {
    vehicleDynamics(State, PrevState);
    thrustInfo(State);
    pidController(State, PrevState);
    TVC(State, PrevState);
    processTVC(State);
    state2vec(State, PrevState, stateVector);

    // Set "prev" values for next timestep
    PrevState = State;
    State.time += State.stepSize;

    if ((State.z < 0.0) && (State.thrustFiring == 2)) {
      write2CSV(stateVector, State);
      printSimResults(State);
      init_Vehicle(State);
    }
  } while ((State.z > 0.0) || (State.thrustFiring != 2));

  return 0;
}
#endif
