using Plots
using CSV
using DataFrames

theme(:juno)
plotlyjs()


df = CSV.File("./public/simOut.csv") |> DataFrame
# df = CSV.File("./simOut.csv") |> DataFrame


println(describe(df))

p1 = let 
    a = plot(df.t, [df.ax df.ay df.az], labels = ["ẍ" "ÿ" "z̈"], title = "Acceleration", ylabel = "m/s^2")

    v = plot(df.t, df.vx .* df.vx .+ df.vy .* df.vy .+ df.vz .* df.vz .|> sqrt, labels = "Total Velocity", title = "Total Velocity", ylabel = "m/s", linecolor = "orchid")

    x = plot(df.t, df.z, labels = "Altitude", title = "Altitude", ylabel = "m", xlabel = "time (ms)", linecolor = "chartreuse")


    plot(a, v, x, layout = (3, 1), legend = :outertopright)
end


p2 = let
    ypr = plot(df.t, [df.yaw df.pitch df.roll], label = ["Yaw" "Pitch" "Roll"], ylabel = "Euler Angles (degree)", title = "Vehicle Deflection")

    ω = plot(df.t, [df.yawdot df.pitchdot df.rolldot], label = ["ÿ" "p̈" "r̈"], ylabel = "Angular Velocity (deg/s)", title = "Vehicle Deflection", xlabel = "time (ms)")

    plot(ypr, ω, layout = (2, 1))
end

p3 = plot(df.t, [df.Servo1 df.Servo2], label = ["Yaw" "Pitch"], ylabel = "Servo Position (degree)", xlabel = "time (ms)", title = "Servo Positions")


global i = 1
theme(:ggplot2)
for p in [p1 p2 p3]
    savefig(p, "./public/plot$i.svg")
    savefig(p, "./public/plot$i.html")
    savefig(p, "./public/plot$i.png")
    global i = i + 1
end
