#include <vector>

#ifndef OUTVECTOR_H
#define OUTVECTOR_H

struct outVector {
  int length = 10000; // current sim runs ~5000 steps, x2 just in case

  std::vector<double> x = std::vector<double>(length, 0.0);
  std::vector<double> y = std::vector<double>(length, 0.0);
  std::vector<double> z = std::vector<double>(length, 0.0);

  std::vector<double> vx = std::vector<double>(length, 0.0);
  std::vector<double> vy = std::vector<double>(length, 0.0);
  std::vector<double> vz = std::vector<double>(length, 0.0);

  std::vector<double> ax = std::vector<double>(length, 0.0);
  std::vector<double> ay = std::vector<double>(length, 0.0);
  std::vector<double> az = std::vector<double>(length, 0.0);

  std::vector<double> yaw = std::vector<double>(length, 0.0);
  std::vector<double> pitch = std::vector<double>(length, 0.0);
  std::vector<double> roll = std::vector<double>(length, 0.0);

  std::vector<double> yawdot = std::vector<double>(length, 0.0);
  std::vector<double> pitchdot = std::vector<double>(length, 0.0);
  std::vector<double> rolldot = std::vector<double>(length, 0.0);

  std::vector<double> servo1 = std::vector<double>(length, 0.0);
  std::vector<double> servo2 = std::vector<double>(length, 0.0);

  std::vector<bool> thrustFiring = std::vector<bool>(length, 0.0);

  std::vector<double> PIDx = std::vector<double>(length, 0.0);
  std::vector<double> PIDy = std::vector<double>(length, 0.0);

  std::vector<double> thrust = std::vector<double>(length, 0.0);
};

#endif